package selenium.com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.remote.RemoteWebElement;

public class dragAndDropFun {
	public static void main(String[] args) throws InterruptedException {

System.setProperty("webdriver.chrome.driver","/home/rajat/Downloads/chromedriver_linux64 (4)/chromedriver");
WebDriver driver= new ChromeDriver();
//System.setProperty("webdriver.gecko.driver","/home/rajat/Downloads/geckodriver");
//WebDriver driver= new FirefoxDriver();
driver.get("https://jqueryui.com/droppable/");
driver.switchTo().frame(0);
WebElement sourceElement=driver.findElement(By.id("draggable"));
WebElement targetElement=driver.findElement(By.id("droppable"));
Actions action=new Actions(driver);
Thread.sleep(3000);
action.dragAndDrop(sourceElement, targetElement).build().perform();
driver.close();
}
}