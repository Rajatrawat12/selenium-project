package selenium.com;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class handlingTheControl {
	static WebDriver driver;

	public static void main(String args[]) throws InterruptedException {
		launchBrowser();
		logInId();
		clickToProfile();
		updateProfile();
		backTohomeProfilePage() ;
		logOutTheProfile();

	}
	@Test
	public static void launchBrowser() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","/home/rajat/Downloads/chromedriver_linux64 (4)/chromedriver");
		driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		driver.get("https://www.edureka.co/");
		driver.manage().window().maximize();
	}





	@Test
	public static void logInId() throws InterruptedException {
		driver.findElement(By.xpath("//span[@data-button-name='Login']")).click();
		Actions action=new Actions(driver);
		action.moveToElement(driver.findElement(By.id("si_popup_email")));
		Thread.sleep(1000);
		action.click();
		action.sendKeys("Rawatrajat300@gmail.com");
		Thread.sleep(2000);
		action.build().perform();
		action.moveToElement(driver.findElement(By.id("si_popup_passwd")));
		Thread.sleep(2000);
		action.click();
		action.sendKeys("Rajsmak@123");
		Thread.sleep(2000);
		action.build().perform();
		action.moveToElement(driver.findElement(By.xpath("//button[@class='clik_btn_log btn-block']")));
		Thread.sleep(2000);
		action.click();
		action.build().perform();

	}


	@Test
	public static void clickToProfile() throws InterruptedException {
		Actions action=new Actions(driver);
		driver.findElement(By.className("img30")).click();
		Thread.sleep(2000);
		action.moveToElement(driver.findElement(By.linkText("My Profile")));
		Thread.sleep(2000);
		action.click();
		action.build().perform();
		Thread.sleep(2000);
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("window.scrollBy(0,500)");


	}




	@Test
	public static void updateProfile() throws InterruptedException {
		Actions action=new Actions(driver);
		action.moveToElement(driver.findElement(By.linkText("Career Services")));
		action.click();
		action.build().perform();
		action.moveToElement(driver.findElement(By.id("professional_details")));
		action.click();
		action.build().perform();
		action.moveToElement(driver.findElement(By.xpath("//input[@name='companyName']")));
		action.click();
		action.sendKeys("MountBlue Tecnologies");
		action.build().perform();
		action.moveToElement(driver.findElement(By.xpath("//select[@name='currentjob']")));
		action.click();
		action.build().perform();
		WebElement n = driver.findElement(By.xpath("//select[@name='currentjob']"));
		Select sl = new Select(n);
		sl.selectByValue("Not Applicable - Fresher");
		Thread.sleep(2000);
		action.moveToElement(driver.findElement(By.xpath("//select[@name='currentIndustry']")));
		action.click();
		action.build().perform();
		WebElement m = driver.findElement(By.xpath("//select[@name='currentIndustry']"));
		Select s2 = new Select(m);
		s2.selectByValue("IT-Software / Software Services");
		Thread.sleep(2000);
		action.moveToElement(driver.findElement(By.xpath("//input[@placeholder=\'Enter skills (separated by commas)\']")));
		action.click();
		action.sendKeys("Hardworking");
		action.build().perform();
		Thread.sleep(5000);

	}





	@Test
	public static void backTohomeProfilePage() throws InterruptedException {
		Actions action=new Actions(driver);
		action.moveToElement(driver.findElement(By.xpath("//button[@type=\'submit\']")));
		action.click();
		action.build().perform();
		action.moveToElement(driver.findElement(By.xpath("//button[@class=\'btn onboarding-primary-button pull-right\']")));
		action.click();
		action.build().perform();
		action.moveToElement(driver.findElement(By.xpath("//button[@class=\'btn pull-right onboarding-primary-button\']")));
		action.click();
		action.build().perform();

	}




	@Test
	public static void logOutTheProfile() throws InterruptedException {
		driver.findElement(By.cssSelector(".user_drop>.btn-group>button>img")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[@href=\'https://www.edureka.co/signout\']")).click();
		JavascriptExecutor js1=(JavascriptExecutor)driver;
		js1.executeScript("window.scrollBy(0,1000)");
		Thread.sleep(2000);
		js1.executeScript("window.scrollBy(0,-1000)");
		Thread.sleep(2000);
		driver.close();
		System.out.println("Test was sucessful");

	}
}