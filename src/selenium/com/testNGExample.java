package selenium.com;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class testNGExample {
	
		public String  baseUrl="https://www.amazon.com/";
		String driverPath = "/home/rajat/Downloads/chromedriver_linux64 (4)/chromedriver";
		public WebDriver driver;

		@BeforeTest
		
		
		public void launchBrowser() {
			System.out.println("launch browser");
			System.setProperty("webdriver.chrome.driver",driverPath);
			 driver= new ChromeDriver();
			driver.get(baseUrl);
		//
			
		}
		@Test
		public void verifyTitle() {
//			System.setProperty("webdriver.chrome.driver","/home/rajat/Downloads/chromedriver_linux64 (4)/chromedriver");
//			WebDriver driver= new ChromeDriver();
//			driver.get("https://www.amazon.com/");
		String expectedTitle="Amazon.com. Spend less. Smile more.";
		Assert.assertEquals(expectedTitle, driver.getTitle());
			
		}



@AfterTest
public void terminateBrowser() {
	driver.close();

}
	

}
